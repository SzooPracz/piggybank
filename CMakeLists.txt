cmake_minimum_required(VERSION 3.8.2)
project(PiggyBank)

set(CMAKE_CXX_STANDARD 17)

add_executable(PiggyBank main.cpp asset.cpp asset.h wallet.h deposit.cpp deposit.h)