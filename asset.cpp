//
// Created by wojtek on 26.01.18.
//

#include "asset.h"
#include <iostream>

asset::asset() :
    name("an asset name"),
    value(0),
    amount(0)
{

    std::cout << "created an asset\n";
}

asset & asset::operator+=(float amount) {
    this->amount += amount;
    return * this;
}

asset & asset::operator-=(float amount) {
    this->amount -= amount;
    return * this;
}

const std::string &asset::getName() const {
    return name;
}

void asset::setName(const std::string &name) {
    asset::name = name;
}

float asset::getValue() const {
    return value;
}

void asset::setValue(float value) {
    asset::value = value;
}

float asset::getAmount() const {
    return amount;
}

void asset::setAmount(float amount) {
    asset::amount = amount;
}

