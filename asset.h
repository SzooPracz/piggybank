//
// Created by wojtek on 26.01.18.
//

#ifndef PIGGYBANK_ASSET_H
#define PIGGYBANK_ASSET_H

#include <string>

class asset {
public:
    asset();
    asset & operator+=(float amount);
    asset & operator-=(float amount);

    virtual void printData() {

    }

private:
    std::string name;

public:
    const std::string &getName() const;

    void setName(const std::string &name);

    float getValue() const;

    void setValue(float value);

    float getAmount() const;

    void setAmount(float amount);

private:
    float value;

private:
    float amount;
};


#endif //PIGGYBANK_ASSET_H
