//
// Created by wojtek on 26.01.18.
//

#ifndef PIGGYBANK_DEPOSIT_H
#define PIGGYBANK_DEPOSIT_H


#include "asset.h"

class deposit : public asset {
private:
    float percentage;
public:
    float getPercentage() const;

    void setPercentage(float percentage);

    void printData();

public:
    explicit deposit(float percentage);
};


#endif //PIGGYBANK_DEPOSIT_H
