//
// Created by wojtek on 26.01.18.
//

#ifndef PIGGYBANK_WALLET_H
#define PIGGYBANK_WALLET_H

#include <vector>

template <class T>
class wallet {
private:
    std::vector<T*> elements;
public:
    void addElement(T *elem){
        elements.push_back(elem);
    }

    void printData() {
        for (auto elem : elements){
            elem->printData();
        }
    }

    void saveFile() {

    }

    void readFile() {

    }
};

#endif //PIGGYBANK_WALLET_H
