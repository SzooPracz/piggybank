//
// Created by wojtek on 26.01.18.
//

#include "deposit.h"
#include <iostream>

deposit::deposit(float percentage) : percentage(percentage) {
    std::cout<<"created a new deposit\n";
}

void deposit::printData() {
    std::cout<<"---DEPOSIT---\n";
    std::cout<<"money: "<<getAmount()<<"\n";
    std::cout<<"percentage: "<<getPercentage()<<"\n";
}

float deposit::getPercentage() const {
    return percentage;
}

void deposit::setPercentage(float percentage) {
    deposit::percentage = percentage;
}
